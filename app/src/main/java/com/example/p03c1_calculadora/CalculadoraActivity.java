package com.example.p03c1_calculadora;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class CalculadoraActivity extends AppCompatActivity {
    private Button btnSumar;
    private Button btnRestar;
    private Button btnMultiplicar;
    private Button btnDividir;
    private Button btnLimpiar;
    private Button btnRegresar;
    private TextView lblUsuario;
    private TextView lblResultado;
    private EditText txtNum1;
    private EditText txtNum2;

    private Calculadora calculadora = new Calculadora(0, 0);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);
        iniciarComponentes();

        btnSumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSumar();
            }
        });

        btnRestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnRestar();
            }
        });

        btnMultiplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnMultiplicar();
            }
        });

        btnDividir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnDividir();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnLimpiar();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnRegresar();
            }
        });

        Bundle datos = getIntent().getExtras();
        String usuario = datos.getString("usuario");
        lblUsuario.setText(usuario);
    }

    private void iniciarComponentes() {
        btnSumar = findViewById(R.id.btnSumar);
        btnRestar = findViewById(R.id.btnRestar);
        btnDividir = findViewById(R.id.btnDividir);
        btnMultiplicar = findViewById(R.id.btnMultiplicar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        lblUsuario = findViewById(R.id.lblUsuario);
        lblResultado = findViewById(R.id.lblResultado);
        txtNum1 = findViewById(R.id.txtNum1);
        txtNum2 = findViewById(R.id.txtNum2);
    }

    private void btnSumar() {
        calculadora.setNum1(Integer.parseInt(txtNum1.getText().toString()));
        calculadora.setNum2(Integer.parseInt(txtNum2.getText().toString()));

        int total = calculadora.suma();
        lblResultado.setText("Resultado es: " + total);
    }

    private void btnRestar() {
        calculadora.setNum1(Integer.parseInt(txtNum1.getText().toString()));
        calculadora.setNum2(Integer.parseInt(txtNum2.getText().toString()));

        int total = calculadora.resta();
        lblResultado.setText("Resultado es: " + total);
    }

    private void btnMultiplicar() {
        calculadora.setNum1(Integer.parseInt(txtNum1.getText().toString()));
        calculadora.setNum2(Integer.parseInt(txtNum2.getText().toString()));

        int total = calculadora.multiplicacion();
        lblResultado.setText("Resultado es: " + total);
    }

    private void btnDividir() {
        calculadora.setNum1(Integer.parseInt(txtNum1.getText().toString()));
        calculadora.setNum2(Integer.parseInt(txtNum2.getText().toString()));

        int total = calculadora.division();
        lblResultado.setText("Resultado es: " + total);
    }

    private void btnLimpiar() {
        txtNum1.setText("");
        txtNum2.setText("");
        lblResultado.setText(".... --- .-.. .-  / .-- . -. .- ...  /");
    }

    private void btnRegresar() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Calculadora");
        confirmar.setMessage("Regresar al MainActivity");
        confirmar.setPositiveButton("Confirmar", (dialogInterface, which) -> finish());
        confirmar.setNegativeButton("Cancelar", (dialogInterface, which) -> {
        });
        confirmar.show();
    }
}

