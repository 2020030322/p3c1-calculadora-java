package com.example.p03c1_calculadora;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button btnIngresar;
    private Button btnSalir;
    private EditText txtUsuario;
    private EditText txtContraseña;

    public MainActivity() {
    }
    public MainActivity(MainActivity main) {
        this.btnIngresar = main.btnIngresar;
        this.btnSalir = main.btnSalir;
        this.txtUsuario = main.txtUsuario;
        this.txtContraseña = main.txtContraseña;
    }

    public MainActivity(Button btnIngresar, Button btnSalir, EditText txtUsuario, EditText txtContraseña) {
        this.btnIngresar = btnIngresar;
        this.btnSalir = btnSalir;
        this.txtUsuario = txtUsuario;
        this.txtContraseña = txtContraseña;
    }
    public Button getBtnIngresar() {
        return btnIngresar;
    }

    public void setBtnIngresar(Button btnIngresar) {
        this.btnIngresar = btnIngresar;
    }

    public Button getBtnSalir() {
        return btnSalir;
    }

    public void setBtnSalir(Button btnSalir) {
        this.btnSalir = btnSalir;
    }

    public EditText getTxtUsuario() {
        return txtUsuario;
    }

    public void setTxtUsuario(EditText txtUsuario) {
        this.txtUsuario = txtUsuario;
    }

    public EditText gettxtContraseña() {
        return txtContraseña;
    }

    public void settxtContraseña(EditText txtContraseña) {this.txtContraseña = txtContraseña;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingresar();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salir();
            }
        });
    }

    private void iniciarComponentes() {
        btnIngresar = findViewById(R.id.btnIngresar);
        btnSalir = findViewById(R.id.btnSalir);
        txtUsuario = findViewById(R.id.txtUsuario);
        txtContraseña = findViewById(R.id.txtContraseña);
    }

    private void ingresar() {
        String strUsuario;
        String strContraseña;

        strUsuario = getResources().getString(R.string.usuario);
        strContraseña = getResources().getString(R.string.contraseña);

        if (strUsuario.equals(txtUsuario.getText().toString()) && strContraseña.equals(txtContraseña.getText().toString())) {
            Bundle bundle = new Bundle();
            bundle.putString("usuario", txtUsuario.getText().toString());

            Intent intent = new Intent(MainActivity.this, CalculadoraActivity.class);
            intent.putExtras(bundle);

            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), "Usuario y/o Contraseña Incorrectos", Toast.LENGTH_SHORT).show();
        }
    }

    private void salir() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Inicio");
        confirmar.setMessage("Quieres salir??");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        confirmar.show();
    }
}